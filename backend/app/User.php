<?php

namespace App;

use App\Util\Validate;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome', 'cpf', 'email', 'password', 'cargo', 'nascimento', 'cep', 'endereco', 'type', 'gerente',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $dates = ['deleted_at'];

    public function index()
    {
        return parent::where('gerente', '<>', '')->get();
    }

    public function getGerentes()
    {
        return parent::whereNull('gerente')->get();
    }

    public function create($fields)
    {
        if (!$this->isGerente()) {
            throw new \Exception('É necessário estar logado como Gerente para essa funcionalidade.', -403);
        }

        $validate = new Validate;
        if (!$validate->cpf($fields['cpf'])) {
            throw new \Exception('O CPF informado é inválido.', -403);
        }

        $nascimento = Carbon::parse(str_replace("/", "-", $fields['nascimento']));

        return parent::create([
            'nome' => $fields['nome'],
            'cpf' => $fields['cpf'],
            'email' => $fields['email'],
            'password' => Hash::make($fields['password']),
            'cargo' => $fields['cargo'] ? $fields['cargo'] : 'Não informado',
            'nascimento' => $nascimento->format('Y-m-d'),
            'cep' => $fields['cep'],
            'endereco' => $fields['endereco'],
            'gerente' => JWTAuth::user()->id,
        ]);
    }

    public function show($id)
    {
        $show = parent::where('id', $id)->first();

        if (!$show) {
            throw new \Exception('Usuário não existe ou foi deletado.', -404);
        }

        return $show;
    }

    public function login($credentials)
    {
        if (!$token = JWTAuth::attempt($credentials)) {
            throw new \Exception('Credencias incorretas, verifique-as e tente novamente.', -403);
        }

        $obj = [
            'id' => JWTAuth::user()->id,
            'nome' => JWTAuth::user()->nome,
            "cpf" => JWTAuth::user()->cpf,
            'email' => JWTAuth::user()->email,
            "cargo" => JWTAuth::user()->cargo,
            "nascimento" => JWTAuth::user()->nascimento,
            "cep" => JWTAuth::user()->cep,
            "endereco" => JWTAuth::user()->endereco,
            "gerente" => JWTAuth::user()->gerente,
            'token' => $token,
        ];

        return $obj;
    }

    public function updateUser($fields, $id)
    {
        $user = $this->show($id);

        if (isset($fields['password'])) {
            $fields['password'] = Hash::make($fields['password']);
        }

        if (empty($fields['password'])) {
            $fields['password'] = $user->password;
        }

        if (isset($fields['nascimento'])) {
            $nascimento = Carbon::parse(str_replace("/", "-", $fields['nascimento']));
            $fields['nascimento'] = $nascimento->format('Y-m-d');
        }

        $user->update($fields);
        return $user;
    }

    public function deleteUser($id)
    {
        if (!$this->isGerente()) {
            throw new \Exception('É necessário estar logado como Gerente para essa funcionalidade.', -403);
        }

        $user = $this->show($id);

        $user->delete();
        return parent::where('gerente', '<>', '')->get();
    }

    public function endereco($cep)
    {
        if (strlen($cep) != 8) {
            throw new \Exception('CEP precisa ter 8 digitos.', -403);
        }

        $url = "https://viacep.com.br/ws/$cep/json/";
        $client = new Client();

        $response = $client->get($url);
        $result = $response->getBody();

        return $result;
    }

    public function logout($token)
    {
        if (!JWTAuth::invalidate($token)) {
            throw new \Exception('Erro. Tente novamente.', -404);
        }
    }

    public function isGerente()
    {
        return empty(JWTAuth::user()->gerente);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function registro()
    {
        return $this->hasMany('App\Registro', 'user_id', 'id');
    }

}
