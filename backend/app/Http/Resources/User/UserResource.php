<?php

namespace App\Http\Resources\User;

use App\Services\ResponseService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * @var
     */
    private $config;

    /**
     * Create a new resource instance.
     *
     * @param  mixed  $resource
     * @return void
     */
    public function __construct($resource, $config = array())
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);

        $this->config = $config;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $gerente = '';
        $gerenteId = '';
        if (!empty($this->gerente)) {
            $user = new User;
            $gerenteObj = $user->show($this->gerente);
            $gerente = $gerenteObj->nome;
            $gerenteId = $gerenteObj->id;
        }

        $nascimento = Carbon::parse($this->nascimento);
        return [
            'id' => $this->id,
            'nome' => $this->nome,
            'cpf' => $this->cpf,
            'email' => $this->email,
            'password' => '********',
            'cargo' => $this->cargo,
            'nascimento' => $nascimento->format('d/m/Y'),
            'cep' => $this->cep,
            'endereco' => $this->endereco,
            'gerente' => $gerente,
            'gerenteId' => $gerenteId,
        ];
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return ResponseService::default($this->config, $this->id);
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param  \Illuminate\Http\Request
     * @param  \Illuminate\Http\Response
     * @return void
     */
    public function withResponse($request, $response)
    {
        $response->setStatusCode(200);
    }
}
