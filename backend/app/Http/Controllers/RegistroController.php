<?php

namespace App\Http\Controllers;

use App\Http\Resources\Registro\RegistroResource;
use App\Http\Resources\Registro\RegistroResourceCollection;
use App\Registro;
use App\Services\ResponseService;
use Illuminate\Http\Request;

class RegistroController extends Controller
{
    private $registro;

    public function __construct(Registro $registro)
    {
        $this->registro = $registro;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        try {
            $registro = $this
                ->registro
                ->create();
        } catch (\Throwable | \Exception $e) {
            return ResponseService::exception('registros.store', null, $e);
        }

        return new RegistroResource($registro, array('type' => 'store', 'route' => 'registros.store'));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $registro = $this
                ->registro
                ->show($id);
        } catch (\Throwable | \Exception $e) {
            return ResponseService::exception('registros.show', $id, $e);
        }

        return new RegistroResource($registro, array('type' => 'show', 'route' => 'registros.show'));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getWithFilter(Request $request)
    {
        try {
            $registros = $this
                ->registro
                ->getWithFilter($request->all());
        } catch (\Throwable | \Exception $e) {
            return ResponseService::exception('registros.filter', null, $e);
        }

        return new RegistroResourceCollection($registros);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDaily($id)
    {
        try {
            $registros = $this
                ->registro
                ->getDaily($id);
        } catch (\Throwable | \Exception $e) {
            return ResponseService::exception('registros.daily', $id, $e);
        }

        return new RegistroResourceCollection($registros);
    }
}
