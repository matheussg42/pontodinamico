<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\LoginUser;
use App\Http\Requests\User\StoreUser;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\User\UserResourceCollection;
use App\Services\ResponseService;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new UserResourceCollection($this->user->index());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        try {
            $user = $this
                ->user
                ->create($request->all());
        } catch (\Throwable | \Exception $e) {
            return ResponseService::exception('users.store', null, $e);
        }

        return new UserResource($user, array('type' => 'store', 'route' => 'users.store'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = $this
                ->user
                ->show($id);
        } catch (\Throwable | \Exception $e) {
            return ResponseService::exception('users.show', $id, $e);
        }

        return new UserResource($user, array('type' => 'show', 'route' => 'users.show'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getGerentes()
    {
        return new UserResourceCollection($this->user->getGerentes());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $this
                ->user
                ->updateUser($request->all(), $id);
        } catch (\Throwable | \Exception $e) {
            return ResponseService::exception('users.update', $id, $e);
        }

        return new UserResource($data, array('type' => 'update', 'route' => 'users.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this
                ->user
                ->deleteUser($id);
        } catch (\Throwable | \Exception $e) {
            return ResponseService::exception('users.destroy', $id, $e);
        }

        return new UserResourceCollection($data);
    }

    /**
     * Login the user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginUser $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            $user = $this
                ->user
                ->login($credentials);
        } catch (\Throwable | \Exception $e) {
            return ResponseService::exception('users.login', null, $e);
        }

        return response()->json($user);
    }

    /**
     * Logout user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        try {
            $this
                ->user
                ->logout($request->input('token'));
        } catch (\Throwable | \Exception $e) {
            return ResponseService::exception('users.logout', null, $e);
        }

        return response(['status' => true, 'msg' => 'Deslogado com sucesso'], 200);
    }

    public function getEnderecoByCEP($cep)
    {
        try {
            $endereco = $this
                ->user
                ->endereco($cep);
        } catch (\Throwable | \Exception $e) {
            return ResponseService::exception('users.endereco', $cep, $e);
        }

        return $endereco;
    }
}
