<?php

namespace App;

use App\User;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Registro extends Model
{

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'created_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $dates = ['deleted_at'];

    public function create()
    {
        $registro = auth()
            ->user()
            ->Registro()
            ->create();

        $obj = ['registro' => $registro->id];
        $results = $this->queryRegistro($obj);

        return $results[0];
    }

    public function show($id)
    {
        $obj = ['registro' => $id];

        $show = $this->queryRegistro($obj);
        return $show[0];
    }

    public function getWithFilter($fields)
    {
        $user = new User;
        if (!$user->isGerente()) {
            throw new \Exception('É necessário estar logado como Gerente para essa funcionalidade.', -403);
        }

        $results = $this->queryRegistro($fields);
        return $results;
    }

    public function getDaily($id)
    {
        $obj = [
            'usuario' => $id,
            'data_ini' => date("Y-m-d"),
            'data_fim' => date('Y-m-d'),
        ];

        $show = $this->queryRegistro($obj);
        return $show;
    }

    public function queryRegistro($fields)
    {
        $dataIni = isset($fields['data_ini']) && $fields['data_ini'] ? " AND r.created_at > '{$fields['data_ini']}' " : '';
        $dataFim = isset($fields['data_fim']) && $fields['data_fim'] ? " AND r.created_at < DATE_ADD('{$fields['data_fim']}', INTERVAL 1 DAY) " : '';
        $usuario = isset($fields['usuario']) && $fields['usuario'] ? " AND u.id = {$fields['usuario']} " : '';
        $gerente = isset($fields['gerente']) && $fields['gerente'] ? " AND g.id = {$fields['gerente']} " : '';
        $registro = isset($fields['registro']) && $fields['registro'] ? " AND r.id = {$fields['registro']} " : '';

        $results = DB::select("SELECT
                r.id as id,
                u.nome,
                u.cargo,
                YEAR(CURDATE()) - YEAR(u.nascimento) AS idade,
                g.nome as gerente,
                DATE_FORMAT(r.created_at, '%d/%m/%Y %H:%i:%s') as registro
            FROM users u
                INNER JOIN registros r on r.user_id =u.id
                LEFT JOIN users g on g.id = u.gerente
            WHERE
                u.deleted_at is null
                {$registro}
                {$dataIni}
                {$dataFim}
                {$usuario}
                {$gerente}
            ORDER BY DATE_FORMAT(r.created_at, '%Y-%m-%d') DESC, u.id, r.created_at desc
        ");

        return $results;
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
