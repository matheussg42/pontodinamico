<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('login', 'UserController@login')->name('users.login');
Route::group(['prefix' => 'v1', 'middleware' => 'jwt.verify'], function () {
    Route::apiResources([
        'users' => 'UserController',
        'registros' => 'RegistroController',
    ]);

    Route::get('endereco/{cep}', 'UserController@getEnderecoByCEP')->name('users.endereco');
    Route::post('registros/getWithFilter', 'RegistroController@getWithFilter')->name('registros.filter');
    Route::get('registros/getDaily/{id}', 'RegistroController@getDaily')->name('registros.daily');
    Route::get('gerentes', 'UserController@getGerentes')->name('users.gerente');
});
