<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */
$factory->define(User::class, function (Faker $faker) {

    $cargos = [
        1 => 'Analista',
        2 => 'Suporte',
        3 => 'Desenvolvedor',
        4 => 'RH',
        5 => 'Comercial',
    ];

    $end = [
        ['cep' => '58428-228', 'end' => 'Bairro Centenário - Rua Travessa Aurita Aires Guimarães, Campina Grande/Paraíba'],
        ['cep' => '21922-530', 'end' => 'Bairro Guarabu - Rua Dezenove de Novembro, Rio de Janeiro/Rio de Janeiro'],
        ['cep' => '97546-340', 'end' => 'Bairro Ibirapuitã - Rua João Pedro de Souza, Alegrete/Rio Grande do Sul'],
        ['cep' => '58015-540', 'end' => 'Bairro Jaguaribe - Rua Doutor José Galvão de Melo, João Pessoa/Paraíba'],
        ['cep' => '88107-470', 'end' => 'Bairro Forquilhas - Rua José Aguiar Madeira, São José/Santa Catarina'],
    ];

    $endAleatorio = Arr::random($end);

    $timestamp = mt_rand(1, time());
    $dataAleatoria = date("Y-m-d", $timestamp);

    return [
        'nome' => $faker->name,
        'cpf' => $faker->unique()->cpf,
        'email' => $faker->unique()->safeEmail,
        'password' => Hash::make('12345'),
        'cargo' => Arr::random($cargos),
        'nascimento' => $dataAleatoria,
        'cep' => $endAleatorio['cep'],
        'endereco' => $endAleatorio['end'],
    ];

});
