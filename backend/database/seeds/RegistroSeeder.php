<?php

use Illuminate\Database\Seeder;

class RegistroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('registros')->insert([
            "user_id" => 2,
            "created_at" => "2020-07-27 08:00:00",
            "updated_at" => "2020-07-27 08:00:00",
        ]);
        DB::table('registros')->insert([
            "user_id" => 2,
            "created_at" => "2020-07-28 08:00:00",
            "updated_at" => "2020-07-28 08:00:00",
        ]);

        DB::table('registros')->insert([
            "user_id" => 2,
            "created_at" => "2020-07-28 12:00:00",
            "updated_at" => "2020-07-28 12:00:00",
        ]);

        DB::table('registros')->insert([
            "user_id" => 2,
            "created_at" => "2020-07-28 13:00:00",
            "updated_at" => "2020-07-28 13:00:00",
        ]);

        DB::table('registros')->insert([
            "user_id" => 2,
            "created_at" => "2020-07-28 17:00:00",
            "updated_at" => "2020-07-28 17:00:00",
        ]);

        DB::table('registros')->insert([
            "user_id" => 3,
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
        ]);
    }
}
