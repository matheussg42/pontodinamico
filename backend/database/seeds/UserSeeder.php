<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $end = [
            ['cep' => '58428-228', 'end' => 'Bairro Centenário - Rua Travessa Aurita Aires Guimarães, Campina Grande/Paraíba'],
            ['cep' => '21922-530', 'end' => 'Bairro Guarabu - Rua Dezenove de Novembro, Rio de Janeiro/Rio de Janeiro'],
            ['cep' => '97546-340', 'end' => 'Bairro Ibirapuitã - Rua João Pedro de Souza, Alegrete/Rio Grande do Sul'],
            ['cep' => '58015-540', 'end' => 'Bairro Jaguaribe - Rua Doutor José Galvão de Melo, João Pessoa/Paraíba'],
            ['cep' => '88107-470', 'end' => 'Bairro Forquilhas - Rua José Aguiar Madeira, São José/Santa Catarina'],
        ];

        $endAleatorio = Arr::random($end);

        //CADASTRO DE GERENTE
        DB::table('users')->insert([
            'nome' => $faker->name,
            'cpf' => $faker->unique()->cpf,
            'email' => 'gerente@email.com.br',
            'password' => Hash::make('12345'),
            'cargo' => 'Gerente Desenvolvimento',
            'nascimento' => date("Y-m-d", mt_rand(1, time())),
            'cep' => $endAleatorio['cep'],
            'endereco' => $endAleatorio['end'],
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
        ]);

        //CADASTRO DE FUNCTIONARIO
        DB::table('users')->insert([
            'nome' => $faker->name,
            'cpf' => $faker->unique()->cpf,
            'email' => 'dev@email.com.br',
            'password' => Hash::make('12345'),
            'cargo' => 'Dev',
            'nascimento' => date("Y-m-d", mt_rand(1, time())),
            'cep' => $endAleatorio['cep'],
            'endereco' => $endAleatorio['end'],
            'gerente' => 1,
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
        ]);

        //CADASTRO DE FUNCTIONARIO DELETADO
        DB::table('users')->insert([
            'nome' => $faker->name,
            'cpf' => $faker->unique()->cpf,
            'email' => 'dev_deletado@email.com.br',
            'password' => Hash::make('12345'),
            'cargo' => 'Dev',
            'nascimento' => date("Y-m-d", mt_rand(1, time())),
            'cep' => $endAleatorio['cep'],
            'endereco' => $endAleatorio['end'],
            'gerente' => 1,
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
            "deleted_at" => date("Y-m-d H:i:s"),
        ]);
    }
}
