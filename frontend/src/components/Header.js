import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { FiPower } from 'react-icons/fi';
import { AppBar, Toolbar  } from '@material-ui/core';

export default function Header() {
  const [user] = useState(JSON.parse(localStorage.getItem('user')));
  const history = useHistory();

  if(user.token === '' || user.token === null){
    history.push('/');
  }

  function handleLogout() {
    localStorage.clear();
    history.push('/');
  }

  return (
    <div className="header">
      <AppBar className="menu" position="static">
        <Toolbar>
          <Link to="#" className="menuTitle">
            <h1>PontoDinâmico</h1>
          </Link>

          {!user.gerente ? (
            <React.Fragment>
              <Link to="/gerente/registros" className="menuItem">
                <p>Registros</p>
              </Link>
              <Link to="/gerente/funcionarios" className="menuItem">
                <p>Funcionarios</p>
              </Link>
              <Link 
                to={{
                  pathname: "/usuario/perfil",
                  state: { 'id': user.id }
                }} className="menuItem">
                <p>Perfil</p>
              </Link>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Link to="/funcionario/ponto" className="menuItem">
                <p>Ponto</p>
              </Link>
              <Link 
                to={{
                  pathname: "/usuario/perfil",
                  state: { 'id': user.id }
                }} className="menuItem">
                <p>Perfil</p>
              </Link>
              {/* <Link to="/usuario/perfil" className="menuItem">
                <p>Perfil</p>
              </Link> */}
            </React.Fragment>
          )}

          <button className="menuButton" onClick={handleLogout} type="button">
            <FiPower size={18} color="#fff" />
          </button>
        </Toolbar>
      </AppBar>
    </div>
  );
}