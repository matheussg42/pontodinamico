import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Header from '../../../components/Header'
import { Container, Grid, TextField, Select, MenuItem  } from '@material-ui/core';
import { store } from 'react-notifications-component';
import api from '../../../services/api';
import './styles.css';

export default function Perfil(props) {
  const [user] = useState(JSON.parse(localStorage.getItem('user')));
  const [funcNome, setFuncNome] = useState('');
  const [funcEmail, setFuncEmail] = useState('');
  const [funcCPF, setFuncCPF] = useState('');
  const [funcCargo, setFuncCargo] = useState('');
  const [funcGerente, setFuncGerente] = useState('');
  const [funcGerenteId, setFuncGerenteId] = useState('');
  const [funcNascimento, setFuncNascimento] = useState('');
  const [funcPassword, setFuncPassword] = useState('');
  const [funcCEP, setFuncCEP] = useState('');
  const [funcEndereco, setFuncEndereco] = useState('');
  const [gerentes, setGerentes] = useState('');
  const idUser = props.location.state.id;

  const history = useHistory();

  useEffect(() => {
    api.get(`/api/v1/users/${idUser}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      if(response.data.status === 401 || response.data.status === 498){
        localStorage.clear();
        history.push('/');
      }
      setFuncNome(response.data.data.nome);
      setFuncEmail(response.data.data.email);
      setFuncCPF(response.data.data.cpf);
      setFuncCargo(response.data.data.cargo);
      setFuncGerente(response.data.data.gerente);
      setFuncGerenteId(response.data.data.gerenteId);
      setFuncNascimento(response.data.data.nascimento);
      setFuncCEP(response.data.data.cep);
      setFuncEndereco(response.data.data.endereco);

    }).catch(err => {
      store.addNotification({
        title: "Erro!",
        message: err.response.data.error,
        type: "danger",
        insert: "top",
        container: "top-right",
        dismiss: {
          duration: 5000,
          onScreen: true
        }
      });
    })
  }, [history, idUser, user.token]);

  useEffect(() => {
    api.get(`/api/v1/gerentes`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      if(response.data.status === 401 || response.data.status === 498){
        localStorage.clear();
        history.push('/');
      }
      setGerentes(response.data.data);
    })
  }, [history, user.token]);

  async function getEndereco(){
    const cep = funcCEP.replace("-", "")

    api.get(`/api/v1/endereco/${cep}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      if(response.data.status === 401 || response.data.status === 498){
        localStorage.clear();
        history.push('/');
      }

      let endereco = `Bairro ${response.data.bairro} - ${response.data.logradouro}, ${response.data.localidade}/${response.data.uf}`;
      setFuncEndereco(endereco);
    })
  }
  
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      'nome': funcNome,
      'email': funcEmail,
      'cpf': funcCPF,
      'cargo': funcCargo,
      'gerente': funcGerenteId,
      'nascimento': funcNascimento,
      'password': funcPassword,
      'cep': funcCEP,
      'endereco': funcEndereco
    };

    api.put(`/api/v1/users/${idUser}`, data, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      if(response.data.status === 401 || response.data.status === 498){
        localStorage.clear();
        history.push('/');
      }

      store.addNotification({
        title: response.data.msg,
        message: `Olá ${response.data.data.nome}, seus dados foram salvos com sucesso!`,
        type: "success",
        insert: "top",
        container: "top-right",
        dismiss: {
          duration: 5000,
          onScreen: true
        }
      });
    }).catch(err => {
      store.addNotification({
        title: "Erro!",
        message: err?.message ? err.message : err,
        type: "danger",
        insert: "top",
        container: "top-right",
        dismiss: {
          duration: 5000,
          onScreen: true
        }
      });
    })
  };

  const handleChangeSelect = (event) => {
    setFuncGerente(event?.target.value);
  };

  const formFunc = !user.gerente ? (
    <React.Fragment>
      <div className="input-group">
        <TextField 
          name="funcNome" 
          id="funcNome" 
          label="Nome"
          className="TextFieldBlock" 
          value={funcNome}
          onChange={e => setFuncNome(e.target.value)}
        />
        <TextField 
          name="funcEmail" 
          id="funcEmail" 
          label="E-mail"
          className="TextFieldBlock" 
          value={funcEmail}
          onChange={e => setFuncEmail(e.target.value)}
        />
        <TextField 
          name="funcCPF" 
          id="funcCPF" 
          label="CPF"
          className="TextFieldBlock" 
          value={funcCPF}
          onChange={e => setFuncCPF(e.target.value)}
        />
      </div>

      <div className="input-group">
        <TextField 
          name="funcCargo" 
          id="funcCargo" 
          label="Cargo"
          className="TextFieldBlock" 
          value={funcCargo}
          onChange={e => setFuncCargo(e.target.value)}
        />
        <Select
          label="Gerente"
          id="funcGerente"
          name="funcGerente" 
          className="TextFieldBlock" 
          value={funcGerenteId}
          onChange={handleChangeSelect}
        >
          {gerentes.length > 0 ? gerentes.map((gerente) => 
            (
              <MenuItem key={gerente.id} value={gerente.id} selected={funcGerenteId === gerente.id} >{gerente.nome}</MenuItem>
            )
          ): null }
        </Select>
        <TextField 
          name="funcNascimento" 
          id="funcNascimento" 
          label="Data de Nascimento"
          className="TextFieldBlock" 
          value={funcNascimento}
          onChange={e => setFuncNascimento(e.target.value)}
        />
      </div>

      <div className="input-group">
        <TextField 
          name="funcPassword" 
          id="funcPassword" 
          label="Senha"
          type="password"
          className="TextFieldBlock" 
          value={funcPassword}
          onChange={e => setFuncPassword(e.target.value)}
        />
        <TextField 
          name="funcCEP" 
          id="funcCEP" 
          label="CEP"
          className="TextFieldBlock" 
          value={funcCEP}
          onChange={e => setFuncCEP(e.target.value)}
          onBlur={e => getEndereco(e)}
        />
        <TextField 
          name="funcEndereco" 
          id="funcEndereco" 
          label="Endereco"
          className="TextFieldBlock" 
          value={funcEndereco}
          onChange={e => setFuncEndereco(e.target.value)}
          disabled={true}
        />
      </div>
    </React.Fragment>
  ) : (
    <TextField 
      name="funcPassword" 
      id="funcPassword" 
      label="Senha"
      type="password"
      className="TextFieldBlock" 
      value={funcPassword}
      onChange={e => setFuncPassword(e.target.value)}
    />
  );

  return (
    <React.Fragment>
      <Header />

      <Container maxWidth="xl">
        <Grid container>
          <div className="form RegistrarUsuario">
            <strong>Editar Informações</strong>

            <form noValidate autoComplete="off" onSubmit={handleSubmit}>
              
              {formFunc}
              <button type="submit">Salvar</button>
            </form>
          </div>

        </Grid>

        <Grid>
          <div className="ExibeUser">
            <Grid container>
              <Grid item xs={4}>
                <p>Nome: {funcNome}</p>
              </Grid>
              <Grid item xs={4}>
                <p>E-mail: {funcEmail}</p>
              </Grid>
              <Grid item xs={4}>
                <p>CPF: {funcCPF}</p>
              </Grid>
              <Grid item xs={4}>
                <p>Cargo: {funcCargo}</p>
              </Grid>
              <Grid item xs={4}>
                <p>Gerente: {funcGerente}</p>
              </Grid>
              <Grid item xs={4}>
                <p>Data de Nascimento: {funcNascimento}</p>
              </Grid>
              <Grid item xs={4}>
                <p>CEP: {funcCEP}</p>
              </Grid>
              <Grid item xs={8}>
                <p>Endereco: {funcEndereco}</p>
              </Grid>
            </Grid>
          </div>
        </Grid>

      </Container>
    </React.Fragment>
  );
}