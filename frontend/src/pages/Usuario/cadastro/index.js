import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { store } from 'react-notifications-component';
import Header from '../../../components/Header'
import { Container, Grid, TextField, Select, MenuItem  } from '@material-ui/core';
import api from '../../../services/api';
import { cpfMask } from '../../../Util/Format'
import './styles.css';

export default function Perfil() {
  const [user] = useState(JSON.parse(localStorage.getItem('user')));
  const [funcNome, setFuncNome] = useState('');
  const [funcEmail, setFuncEmail] = useState('');
  const [funcCPF, setFuncCPF] = useState('');
  const [funcCargo, setFuncCargo] = useState('');
  const [funcGerenteId, setFuncGerenteId] = useState('');
  const [funcNascimento, setFuncNascimento] = useState('');
  const [funcPassword, setFuncPassword] = useState('');
  const [funcCEP, setFuncCEP] = useState('');
  const [funcEndereco, setFuncEndereco] = useState('');
  const [gerentes, setGerentes] = useState('');

  const history = useHistory();

  useEffect(() => {
    api.get(`/api/v1/gerentes`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      if(response.data.status === 401 || response.data.status === 498){
        localStorage.clear();
        history.push('/');
      }
      setGerentes(response.data.data);
    })
  }, [history, user.token]);

  async function getEndereco(){
    const cep = funcCEP.replace("-", "")

    api.get(`/api/v1/endereco/${cep}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      if(response.data.status === 401 || response.data.status === 498){
        localStorage.clear();
        history.push('/');
      }

      let endereco = `Bairro ${response.data.bairro} - ${response.data.logradouro}, ${response.data.localidade}/${response.data.uf}`;
      setFuncEndereco(endereco);
    })
  }
  
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      'nome': funcNome,
      'email': funcEmail,
      'cpf': funcCPF,
      'cargo': funcCargo,
      'gerente': funcGerenteId,
      'nascimento': funcNascimento,
      'password': funcPassword,
      'cep': funcCEP,
      'endereco': funcEndereco
    };

    api.post(`/api/v1/users`, data, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      if(response.data.status === 401 || response.data.status === 498){
        localStorage.clear();
        history.push('/');
      }

      store.addNotification({
        title: "Sucesso!",
        message: `O Funcionário ${funcNome} foi cadastrado com sucesso!`,
        type: "success",
        insert: "top",
        container: "top-right",
        dismiss: {
          duration: 10000,
          onScreen: true
        }
      });

      setFuncNome('');
      setFuncEmail('');
      setFuncCPF('');
      setFuncCargo('');
      setFuncGerenteId('');
      setFuncNascimento('');
      setFuncPassword('');
      setFuncCEP('');
      setFuncEndereco('');
    }).catch(err => {
      if(err.response.data.errors && Object.keys(err.response.data.errors).length > 0){
        for(let erro of Object.values(err.response.data.errors)){
          store.addNotification({
            title: "Erro!",
            message: erro[0],
            type: "danger",
            insert: "top",
            container: "top-right",
            dismiss: {
              duration: 10000,
              onScreen: true
            }
          });
        }
      }else{
        store.addNotification({
          title: "Erro!",
          message: err.response.data.error,
          type: "danger",
          insert: "top",
          container: "top-right",
          dismiss: {
            duration: 5000,
            onScreen: true
          }
        });
      }
    })
  };

  const handleChangeSelect = (event) => {
    setFuncGerenteId(event?.target.value);
  };

  return (
    <React.Fragment>
      <Header />

      <Container maxWidth="xl">
        <Grid container>
          <div className="form RegistrarUsuario">
            <strong>Editar Informações</strong>

            <form noValidate autoComplete="off" onSubmit={handleSubmit}>
              
              <div className="input-group">
                <TextField 
                  name="funcNome" 
                  id="funcNome" 
                  label="Nome"
                  className="TextFieldBlock" 
                  value={funcNome}
                  onChange={e => setFuncNome(e.target.value)}
                  required
                />
                <TextField 
                  name="funcEmail" 
                  id="funcEmail" 
                  label="E-mail"
                  className="TextFieldBlock" 
                  value={funcEmail}
                  onChange={e => setFuncEmail(e.target.value)}
                  required
                />
                <TextField 
                  name="funcCPF" 
                  id="funcCPF" 
                  label="CPF"
                  className="TextFieldBlock" 
                  value={funcCPF}
                  onChange={e => setFuncCPF(cpfMask(e.target.value))}
                  required
                />
              </div>

              <div className="input-group">
                <TextField 
                  name="funcCargo" 
                  id="funcCargo" 
                  label="Cargo"
                  className="TextFieldBlock" 
                  value={funcCargo}
                  onChange={e => setFuncCargo(e.target.value)}
                />
                <Select
                  label="Gerente"
                  id="funcGerente"
                  name="funcGerente" 
                  className="TextFieldBlock" 
                  value={funcGerenteId}
                  onChange={handleChangeSelect}
                >
                  {gerentes.length > 0 ? gerentes.map((gerente) => 
                    (
                      <MenuItem key={gerente.id} value={gerente.id} selected={funcGerenteId === gerente.id} >{gerente.nome}</MenuItem>
                    )
                  ): null }
                </Select>
                <TextField
                  name="funcNascimento" 
                  id="funcNascimento" 
                  label="Data de Nascimento"
                  className="TextFieldBlock" 
                  type="date"
                  value={funcNascimento}
                  onChange={e => setFuncNascimento(e.target.value)}
                  required
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </div>

              <div className="input-group">
                <TextField 
                  name="funcPassword" 
                  id="funcPassword" 
                  label="Senha"
                  type="password"
                  className="TextFieldBlock" 
                  value={funcPassword}
                  onChange={e => setFuncPassword(e.target.value)}
                  required
                />
                <TextField 
                  name="funcCEP" 
                  id="funcCEP" 
                  label="CEP"
                  className="TextFieldBlock" 
                  value={funcCEP}
                  onChange={e => setFuncCEP(e.target.value)}
                  onBlur={e => getEndereco(e)}
                  required
                />
                <TextField 
                  name="funcEndereco" 
                  id="funcEndereco" 
                  label="Endereco"
                  className="TextFieldBlock" 
                  value={funcEndereco}
                  onChange={e => setFuncEndereco(e.target.value)}
                  disabled={true}
                />
              </div>
              <button type="submit">Salvar</button>
            </form>
          </div>

        </Grid>
      </Container>
    </React.Fragment>
  );
}