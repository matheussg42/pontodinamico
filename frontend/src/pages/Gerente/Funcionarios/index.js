import React, { useState, useEffect } from 'react';
import {  Link, useHistory } from 'react-router-dom';
import Header from '../../../components/Header'
import { store } from 'react-notifications-component';
import { Container, Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody  } from '@material-ui/core';
import api from '../../../services/api';
import { FiTrash, FiEdit, FiPlus } from 'react-icons/fi';
import './styles.css';

export default function Funcionarios() {
  const [user] = useState(JSON.parse(localStorage.getItem('user')));
  const [funcionarios, setFuncionarios] = useState();

  const history = useHistory();

  useEffect(() => {
    api.get(`/api/v1/users`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      if(response.data.status === 401 || response.data.status === 498){
        localStorage.clear();
        history.push('/');
      }
      setFuncionarios(response.data.data);
    }).catch(err => {
      store.addNotification({
        title: "Erro!",
        message: err.response.data.error,
        type: "danger",
        insert: "top",
        container: "top-right",
        dismiss: {
          duration: 5000,
          onScreen: true
        }
      });
    })
  }, [history, user.token]);

  const handleDelete = (id) => {
    api.delete(`/api/v1/users/${id}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      if(response.data.status === 401 || response.data.status === 498){
        localStorage.clear();
        history.push('/');
      }
      setFuncionarios(response.data.data);
      store.addNotification({
        title: "Informação",
        message: "Funcionário apagado com sucesso.",
        type: "default",
        insert: "top",
        container: "top-right",
        dismiss: {
          duration: 5000,
          onScreen: true
        }
      });
    }).catch(err => {
      store.addNotification({
        title: "Erro!",
        message: err.response.data.error,
        type: "danger",
        insert: "top",
        container: "top-right",
        dismiss: {
          duration: 5000,
          onScreen: true
        }
      });
    })
  };

  return (
    <React.Fragment>
      <Header />


      <Link 
        to={{
          pathname: "/usuario/cadastrar"
        }} className="btnCadastrar">
        <p><FiPlus size={18} /> Cadastrar Usuário</p>
      </Link>
      <Container maxWidth="xl">
        <Grid container>
          <TableContainer className="tableContainer tableGrid">
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">Nº</TableCell>
                  <TableCell align="center">Nome</TableCell>
                  <TableCell align="center">E-mail</TableCell>
                  <TableCell align="center">CPF</TableCell>
                  <TableCell align="center">Cargo</TableCell>
                  <TableCell align="center">Idade</TableCell>
                  <TableCell align="center">Endereço</TableCell>
                  <TableCell align="center">Gerente</TableCell>
                  <TableCell align="center">Ações</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {funcionarios?.length > 0 ? funcionarios.map((funcionario) => 
                  (
                    <TableRow key={funcionario.id}>
                      <TableCell align="center">{funcionario.id}</TableCell>
                      <TableCell align="center">{funcionario.nome}</TableCell>
                      <TableCell align="center">{funcionario.email}</TableCell>
                      <TableCell align="center">{funcionario.cpf}</TableCell>
                      <TableCell align="center">{funcionario.cargo}</TableCell>
                      <TableCell align="center">{funcionario.nascimento}</TableCell>
                      <TableCell align="center">{funcionario.endereco} - {funcionario.cep}</TableCell>
                      <TableCell align="center">{funcionario.gerente}</TableCell>
                      <TableCell align="center">
                        <Link 
                          to={{
                            pathname: "/usuario/perfil",
                            state: { 'id': funcionario.id }
                          }}>
                          <FiEdit className="deleteIcon" size={18} />
                        </Link>
                        <FiTrash className="deleteIcon" onClick={() => handleDelete(funcionario.id)} size={18} />
                      </TableCell>
                    </TableRow>
                  )) : null 
                }
              </TableBody>
            </Table>
          </TableContainer>

        </Grid>
      </Container>
    </React.Fragment>
  );
}