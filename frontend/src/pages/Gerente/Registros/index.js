import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Header from '../../../components/Header'
import { Container, Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, TextField, Select, MenuItem, FormControl, InputLabel  } from '@material-ui/core';
import api from '../../../services/api';
import './styles.css';

export default function Ponto() {
  const [user] = useState(JSON.parse(localStorage.getItem('user')));
  const [registros, setRegistros] = useState();
  const [dataIniFiltro, setDataIniFiltro] = useState();
  const [dataFimFiltro, setDataFimFiltro] = useState();
  const [gerenteFiltro, setGerenteFiltro] = useState();
  const [funcionarioFiltro, setFuncionarioFiltro] = useState();
  const [gerentes, setGerentes] = useState();
  const [funcionarios, setFuncionarios] = useState();

  const history = useHistory();

  useEffect(() => {
    api.get(`/api/v1/gerentes`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      if(response.data.status === 401 || response.data.status === 498){
        localStorage.clear();
        history.push('/');
      }
      setGerentes(response.data.data);
    })
  }, [history, user.token]); 

  useEffect(() => {
    api.get(`/api/v1/users`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      if(response.data.status === 401 || response.data.status === 498){
        localStorage.clear();
        history.push('/');
      }
      setFuncionarios(response.data.data);
    })
  }, [history, user.token]);

  useEffect(() => {
    getRegistros();
  }, []);

  useEffect(() => {
    getRegistros();
  }, [dataIniFiltro, dataFimFiltro, gerenteFiltro, funcionarioFiltro]);

  const getRegistros = async () => {
    const data = {
      "data_ini":dataIniFiltro,
      "data_fim":dataFimFiltro,
      "gerente":gerenteFiltro,
      "usuario":funcionarioFiltro
    }

    api.post(`/api/v1/registros/getWithFilter`, data, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      setRegistros(response.data.data);
    })
  };

  const handleChangeSelect = (event) => {
    setGerenteFiltro(event?.target.value);
  };
  
  const handleChangeSelectFuncionario = (event) => {
    setFuncionarioFiltro(event?.target.value);
  };

  return (
    <React.Fragment>
      <Header />

      <Container maxWidth="xl">
        <Grid container>
          <div className="form filtrarRegistros">
            <div className="input-group">
              <TextField
                name="dataIniFiltro" 
                id="dataIniFiltro" 
                label="Data Inicial"
                type="date"
                value={dataIniFiltro}
                onChange={e => setDataIniFiltro(e.target.value)}
                InputLabelProps={{
                  shrink: true,
                }}
              />

              <TextField
                name="dataFimFiltro" 
                id="dataFimFiltro" 
                label="Data Final"
                type="date"
                value={dataFimFiltro}
                onChange={e => setDataFimFiltro(e.target.value)}
                InputLabelProps={{
                  shrink: true,
                }}
              />

              <FormControl>
                <InputLabel id="select-list-label">Registros por Gerente</InputLabel>
                <Select
                labelId="select-list-label"
                  label="Gerente"
                  id="gerenteFiltro"
                  name="gerenteFiltro" 
                  className="TextFieldBlock" 
                  onChange={handleChangeSelect}
                >
                  <MenuItem key={undefined} value={undefined}>Selecione um Registro</MenuItem>
                  {gerentes?.length > 0 ? gerentes.map((gerente) => 
                    (
                      <MenuItem key={gerente.id} value={gerente.id} >{gerente.nome}</MenuItem>
                    )
                  ): null }
                </Select>
              </FormControl>

              <FormControl>
                <InputLabel id="select-list-label">Registros de Funcionário</InputLabel>
                <Select
                labelId="select-list-label"
                  label="Funcionário"
                  id="funcionarioFiltro"
                  name="funcionarioFiltro" 
                  className="TextFieldBlock" 
                  onChange={handleChangeSelectFuncionario}
                >
                  <MenuItem key={undefined} value={undefined}>Selecione um Registro</MenuItem>
                  {funcionarios?.length > 0 ? funcionarios.map((funcionario) => 
                    (
                      <MenuItem key={funcionario.id} value={funcionario.id} >{funcionario.nome}</MenuItem>
                    )
                  ): null }
                </Select>
              </FormControl>
              
            </div>
          </div>
        </Grid>
      </Container>

      <Container maxWidth="xl">
        <Grid container>
          <TableContainer className="tableContainer tableGrid">
            <h3 className="tableTitle">Registros De Ponto</h3>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">Nº Registro</TableCell>
                  <TableCell align="center">Nome</TableCell>
                  <TableCell align="center">Cargo</TableCell>
                  <TableCell align="center">Idade</TableCell>
                  <TableCell align="center">Gerente</TableCell>
                  <TableCell align="center">Horario</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {registros?.length > 0 ? registros.map((registro) => 
                  (
                    <TableRow key={registro.id}>
                      <TableCell align="center">{registro.id}</TableCell>
                      <TableCell align="center">{registro.nome}</TableCell>
                      <TableCell align="center">{registro.cargo}</TableCell>
                      <TableCell align="center">{registro.idade} anos</TableCell>
                      <TableCell align="center">{registro.gerente}</TableCell>
                      <TableCell align="center">{registro.registro}</TableCell>
                    </TableRow>
                  )) : null 
                }
              </TableBody>
            </Table>
          </TableContainer>

        </Grid>
      </Container>
    </React.Fragment>
  );
}