import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { store } from 'react-notifications-component';

import api from '../../services/api';

import './styles.css';

export default function Logon() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const history = useHistory();

  async function handleLogin(e) {
    e.preventDefault();

    try {
      const response = await api.post('api/login', { email, password });
      localStorage.setItem('user', JSON.stringify(response.data));
      if(!response.data.gerente){
        history.push('/gerente/registros');
      }else{
        history.push('/funcionario/ponto');
      }
    } catch (err) {
      console.dir(err)
      if(err.response.data.errors && Object.keys(err.response.data.errors).length > 0){
        for(let erro of Object.values(err.response.data.errors)){
          store.addNotification({
            title: "Erro!",
            message: erro[0],
            type: "danger",
            insert: "top",
            container: "top-right",
            dismiss: {
              duration: 10000,
              onScreen: true
            }
          });
        }
      }else{
        store.addNotification({
          title: "Erro!",
          message: err.response.data.error,
          type: "danger",
          insert: "top",
          container: "top-right",
          dismiss: {
            duration: 5000,
            onScreen: true
          }
        });
      }
    }
  }

  return (
    <div className="logon-container">
      <section className="form">

        <form onSubmit={handleLogin}>
          <input 

            placeholder="Seu e-mail"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
          <input 
            placeholder="Sua Senha"
            type="password"
            value={password}
            onChange={e => setPassword(e.target.value)}
          />

          <button className="button" type="submit">Entrar</button>
        </form>
      </section>
    </div>
  );
}
