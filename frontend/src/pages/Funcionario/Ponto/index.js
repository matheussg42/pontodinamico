import React, { useState, useEffect } from 'react';
import { store } from 'react-notifications-component';
import Header from '../../../components/Header'
import { Container, Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody  } from '@material-ui/core';
import api from '../../../services/api';
import 'react-notifications-component/dist/theme.css'
import './styles.css';

export default function Ponto() {
  const [user] = useState(JSON.parse(localStorage.getItem('user')));
  const [registros, setRegistros] = useState();

  useEffect(() => {
    api.get(`/api/v1/registros/getDaily/${user.id}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      setRegistros(response.data.data);
    })
  }, [user.id, user.token]);

  async function handleSubmit(e){
    e.preventDefault();
    api.post("/api/v1/registros", {}, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      }
    }).then(response => {
      setRegistros([...registros, response.data.data]);
      store.addNotification({
        title: response.data.msg,
        message: `Olá ${response.data.data.nome}, seu ponto foi registrado em ${response.data.data.registro}`,
        type: "success",
        insert: "top",
        container: "top-right",
        dismiss: {
          duration: 5000,
          onScreen: true
        }
      });
    }).catch(err => {
      store.addNotification({
        title: "Erro!",
        message: err?.message ? err.message : err,
        type: "danger",
        insert: "top",
        container: "top-right",
        dismiss: {
          duration: 5000,
          onScreen: true
        }
      });
    })
  }

  return (
    <React.Fragment>
      <Header />

      <Container maxWidth="xl">
        <Grid container>
         
        <div className="form RegistrarPonto">
          <strong>Registrar Ponto</strong>
          <form noValidate autoComplete="off" onSubmit={handleSubmit}>
            <button type="submit">Registar</button>
          </form>
        </div>

        </Grid>
      </Container>

      <Container maxWidth="xl">
        <Grid container>
         
        <TableContainer className="tableContainer tableGrid">
          <h3 className="tableTitle">Registros do dia</h3>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Nº Registro</TableCell>
                <TableCell align="center">Nome</TableCell>
                <TableCell align="center">Cargo</TableCell>
                <TableCell align="center">Idade</TableCell>
                <TableCell align="center">Gerente</TableCell>
                <TableCell align="center">Horario</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {registros?.length > 0 ? registros.map((registro) => 
                (
                  <TableRow key={registro.id}>
                    <TableCell align="center">{registro.id}</TableCell>
                    <TableCell align="center">{registro.nome}</TableCell>
                    <TableCell align="center">{registro.cargo}</TableCell>
                    <TableCell align="center">{registro.idade} anos</TableCell>
                    <TableCell align="center">{registro.gerente}</TableCell>
                    <TableCell align="center">{registro.registro}</TableCell>
                  </TableRow>
                )) : null 
               }
            </TableBody>
          </Table>
        </TableContainer>

        </Grid>
      </Container>
    </React.Fragment>
  );
}