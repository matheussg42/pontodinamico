import React from 'react';

import './global.css';

import ReactNotifications from 'react-notifications-component';
import Routes from './routes';

function App() {
  return (
    <div>
      <ReactNotifications />
      <Routes/>
    </div>
  );
}

export default App;
