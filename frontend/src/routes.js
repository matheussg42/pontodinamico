import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Logon from './pages/Logon';
import Ponto from './pages/Funcionario/Ponto';
import Registros from './pages/Gerente/Registros';
import Funcionarios from './pages/Gerente/Funcionarios';
import Perfil from './pages/Usuario/perfil';
import Cadastro from './pages/Usuario/cadastro';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Logon} />
        <Route path="/funcionario/ponto" exact component={Ponto} />
        <Route path="/gerente/registros" exact component={Registros} />
        <Route path="/gerente/funcionarios" exact component={Funcionarios} />
        <Route path="/usuario/perfil" component={Perfil} />
        <Route path="/usuario/cadastrar" component={Cadastro} />
      </Switch>
    </BrowserRouter>
  );
}