👤 **Matheus S. Gomes** 

* Website: https://matheus.sgomes.dev
* Github: [@Matheussg42](https://github.com/Matheussg42)
* LinkedIn: [@matheussg](https://linkedin.com/in/matheussg)

--

## Tecnologias

Esse projeto foi desenvolvido com as seguintes tecnologias:

- [PHP](https://www.php.net/)
- [Laravel](https://laravel.com/)
- [React](https://reactjs.org)

## Projeto

Para executar o projeto, crie um banco de dados com o nome **_pontodinamico_**. Dentro da pasta **_backend_** execute o comando **php artisan migrate:fresh --seed** para gerar os registros do banco. Ainda no **_backend_** execute o comando **php artisan serve** para iniciar o servidor do Laravel, e então navegue até a pasta **_frontend_** e execute os comandos **_yarn install_** (ou **_npm install_**) e **_yarn start_** (ou **_npm install_**).